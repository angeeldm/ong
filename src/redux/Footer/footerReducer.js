import { Get } from '../../services/HttpService';
// CONSTANTS
const intialState = {
    webLinks: [],
    socialLinks: []
}

// TYPES
const GET_WEB_LINKS = 'GET_WEB_LINKS';
const GET_SOCIAL_LINKS = 'GET_SOCIAL_LINKS';

// REDUCERS

const footerReducer = (state = intialState, action) => {
    switch (action.type) {
        case GET_WEB_LINKS:
            return {...state, webLinks: action.payload}

        case GET_SOCIAL_LINKS:
            return {...state, socialLinks: action.payload}
    
        default:
            return state;
    }
}


// ACTIONS
const getWebLinks = () => async (dispatch) => {
    // const response = await Get('url');
    const response = [
        {path: '/',
         pathname: 'Inicio'
        },
        {path: '/novedad',
         pathname: 'Novedades'
        },
        {path: '/contacto',
         pathname: 'Contacto'
        }
    ]
    dispatch({
        type: GET_WEB_LINKS,
        payload: response
    })
}

const getSocialLinks = () => async (dispatch) => {
    await Get(`/organizations/1/public`)
    .then(res => {
        console.log(res);
        const response = [
            {path: res.instagramUrl,
             pathname: 'Instagram'
            },
            {path: res.linkedInUrl,
             pathname: 'LinkedIn'
            },
            {path: res.facebookUrl,
             pathname: 'Facebook'
            }
        ]
        dispatch({
            type: GET_SOCIAL_LINKS,
            payload: response
        })
    })
    .catch(err => console.log(err))
}

export {footerReducer, getWebLinks, getSocialLinks}