import { Get, Patch } from '../../services/HttpService';

const GET_USER = 'GET_USER'
const EDIT_USER = 'EDIT_USER'
const GET_ALL_USERS = 'GET_ALL_USERS'
const SELECT_USER_FOR_EDIT = 'SELECT_USER_FOR_EDIT'

const initialState = {
  user: [],
  listOfUsers: [],
  userSelected: []

}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_USERS:
      return  { ...state, listOfUsers: action.payload }
    case GET_USER:
      return { ...state, user: action.payload }
    case SELECT_USER_FOR_EDIT:
      return {...state, userSelected: action.payload}
    case EDIT_USER:
      return { ...state, user: action.payload }

    default:
      return state
  }
}

export const get_all_users = (users) => (dispatch) => {
  dispatch({
    type: GET_ALL_USERS,
    payload: users
  })
}

export const select_user_for_edit = (selectedUser) => (dispatch, getState) => {
  // const {user} = getState();
  // const userSelected = user.listOfUsers.filter(user => id === user.id)
  // console.log("userSELECCCT",userSelected)

  dispatch({
    type: SELECT_USER_FOR_EDIT,
    payload: [selectedUser]
  })
}

export const getUser = (token) => async (dispatch) => {
  const response = await Get('http://localhost:3001/users')
    dispatch({
      type: GET_USER,
      // payload: response.data
      payload: response
    });
};

const editUser = (values) => (dispatch) => {
  // const response = await Patch(`users/{values.id}`)
  const response = {
    id: values.id,
    name: values.name,
    lastName: values.lastName,
    roleId: values.roleId,
  }
  dispatch({
    type: EDIT_USER,
    payload: response,
  })
}

export { userReducer, editUser }
