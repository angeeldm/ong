import { Put, Get } from "../../services/HttpService"; 

const initialState = {
  organization: {},
};

const EDIT_ORGANIZATION = "EDIT_ORGANIZATION";
const GET_ORGANIZATION = "GET_ORGANIZATION"

const organizationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORGANIZATION:
      return {
        ...state,
        organization: action.payload
      }
    case EDIT_ORGANIZATION:
      return { ...state, organization: action.payload };

    default:
      return state;
  }
};

const getOrganization = (orgFound) => async (dispatch) => {
  // const organization = await Get(`/organizations/${id}/public`)

  dispatch({
    type: GET_ORGANIZATION,
    payload: orgFound
  })
}

const editOrganization = (values) => async (dispatch) => {
  const response = await Put(`url/${values.id}`);
  dispatch({
    type: EDIT_ORGANIZATION,
    payload: response,
  });
};

export { organizationReducer, editOrganization, getOrganization };
