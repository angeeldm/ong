import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Contact from "./pages/Contact/Contact";
import SignupForm from "./pages/LoginForm/Form";
import NewsDetails from "./pages/NewsDetails/index";
import Layout from "./Components/Layout/index";
import News from "./pages/News/index"
import Testimonials from "./pages/Testimonials/Testimonials";
import CreateTestimonial from "./pages/CreateTestimonial/index"
import EditTestimonial from "./pages/EditTestimonial/index"
import ActivitiesList from "./pages/ActivitiesList/index"
import ActivityForm from "./pages/ActivityForm/ActivityForm";
import RegisterForm from "./Components/RegisterForm/RegisterForm"
import Profile from "./pages/Profile/index"
import EditUserForm from "./pages/EditUserForm/editUserForm"
import NewsForm from "./pages/NewsForm/NewsForm";
import NewsListAdmin from "./pages/NewsListAdmin/NewsListAdmin";
import TableUsers from './Components/TableUsers/TableUsers'
import Home from "./pages/Home/Home";
import Header from "./Components/Header";
import Footer from "./Components/Footer/Footer";

function App() {
  return (
    <>
      <Router>
        <Header />
        <div className="App">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/novedad" component={News} />
            <Route exact path="/contacto" component={Contact} />
            <Route exact path="/iniciar" component={SignupForm} />
            <Route exact path="/registro" component={RegisterForm}/>
            <Route exact path="/perfil/:id" component={Profile}/>
            <Route exact path="/novedad/:id" component={NewsDetails} />
            <Route exact path="/backoffice/novedad" component={NewsListAdmin} />
            <Route exact path="/backoffice/edit-novedad/:id" component={NewsForm} />
            <Route exact path="/backoffice/crear-novedad" component={NewsForm} />
            <Route exact path="/backoffice/editar-usuario/:id" component={EditUserForm} />

            <Route path="/backoffice/testimonios" component={Testimonials} />
            <Route exact path="/backoffice/crear-testimonios" component={CreateTestimonial}/>
            <Route exact path="/backoffice/testimonios/:id" component={EditTestimonial}/>
            <Route exact path="/backoffice/actividades" component={ActivitiesList} />
            <Route exact path="/backoffice/actividades/:id" component={ActivityForm} />
            <Route exact path="/backoffice/editar-perfil" component={EditUserForm} />
            <Route exact path="/backoffice/editar-usuarios" component={TableUsers} />
          </Switch>
        </div>
        <Footer />
      </Router>
    </>
  );
}

export default App
