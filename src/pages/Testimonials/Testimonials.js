import React, { useEffect } from "react";
import {
  TestimonialContainer,
  Title,
  Information,
  ListContainer,
  TestimonialCard,
  TestimonialTitle,
  ButtonContainer,
} from "./Styles";
import { useSelector, useDispatch } from "react-redux";
import {
  deleteTestimonial,
  getTestimonials,
} from "../../redux/Testimonials/testimonialsReducer";
import { Button } from "@material-ui/core";
import { FaTrash } from "react-icons/fa";
import { AiFillEdit } from "react-icons/ai";
import { useHistory } from "react-router-dom";
import Backoffice from "../Backoffice/index";
import { GridContainer, NewImage } from "../NewsListAdmin/Styles";

const Testimonials = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const testimonials = useSelector((store) => store.testimonials.testimonials);

  useEffect(() => {
    dispatch(getTestimonials());
  }, []);

  return (
    <GridContainer>
    <Backoffice />
    <TestimonialContainer>
      <Title>Testimonios</Title>
      <Information>
        En este listado encuentra información sobre los testimoniales, con la opción de editar o eliminar.
      </Information>

      <Button variant="contained" color="primary" style={{margin: '10px'}} onClick={() => history.push('/backoffice/crear-testimonios')}> Agregar Testimonio </Button>

      <ListContainer>
        {testimonials.length > 0 && 
           testimonials.map((testimonial) => {
              return (
                <TestimonialCard key={testimonial.id}>
                  <TestimonialTitle>{testimonial.name}</TestimonialTitle>
                  <img style={{width: "60%", margin: "0 auto"}} src={`/files/getfile/${testimonial.image}`} alt="" />
                  <ButtonContainer>
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => {
                        dispatch(deleteTestimonial(testimonial.id));
                      }}
                    >
                      {" "}
                      <FaTrash />{" "}
                    </Button>
                  </ButtonContainer>
                </TestimonialCard>
              );
            })
          }
      </ListContainer>
    </TestimonialContainer>
    </GridContainer>
  );
};

export default Testimonials;
