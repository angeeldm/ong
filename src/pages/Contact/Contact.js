import React, { useState } from 'react';
import {ContactContainer, MainTitle, GridContainer, ElementTitle, Info, FormContainer, ContactForm} from './Styles';
import {TextField, Button} from '@material-ui/core';
import { Post } from '../../services/HttpService';
import {alertSuccess, alertError, alertInfo} from '../../elements/Alert';
import {useHistory} from 'react-router-dom';

const Contact = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [text, setText] = useState('');
    const history = useHistory();

    const handleChange = (e) => {
        if(e.target.id === 'name'){
            setName(e.target.value);
        } if(e.target.id === 'email'){
            setEmail(e.target.value)
        } if(e.target.id === 'text'){
            setText(e.target.value)
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if(name === '' || email === '' || text === ''){
            alertInfo({title: 'Todos los campos son obligatorios'})
        } else{
            const data = {
                name: name,
                email: email,
                message: text
            }

            try {
                Post("/contacts", data) 
                .then(res => {
                    if(res){
                        alertSuccess({title: 'Gracias!', text: 'Estaremos en contacto contigo'});
                        history.push('/');
                    } else{
                        alertError({title: 'Oops!', text: 'Intenta nuevamente, por favor'});
                    }
                })
            } catch (error) {
                alertError({title: 'Oops!', text: 'Intenta nuevamente, por favor'});
            }
            setName('');
            setEmail('');
            setText('');
        }
    }

    return (
        <ContactContainer>
            <MainTitle>Estemos en contacto</MainTitle>

            <GridContainer>
                <div>
                    <ElementTitle>Nosotros</ElementTitle>
                    <Info>Desde 1997 en Somos Más trabajamos con los chicos y chicas, mamás y papás, abuelos y vecinos del barrio La Cava generando procesos de crecimiento y de inserción social. Uniendo las manos de todas las familias, las que viven en el barrio y las que viven fuera de él, es que podemos pensar, crear y garantizar estos procesos.  Somos una asociación civil sin fines de lucro que se creó en 1997 con la intención de dar alimento a las familias del barrio.</Info>
                </div>

                <FormContainer>
                    <ElementTitle>Llena el formulario</ElementTitle>
                    <ContactForm onSubmit={handleSubmit}>
                        <TextField type="name" id="name" value={name} onChange={handleChange} placeholder="Jhon Doe" variant="outlined" size="small"/>
                        <TextField type="email" id="email" value={email} onChange={handleChange} placeholder="email@example.com" variant="outlined" size="small"/>
                        <TextField type="messagge" id="text" value={text} onChange={handleChange} placeholder="Ingresa el texto" variant="outlined" multiline rows={4}/>
                        <Button type="submit" variant="contained" color="primary" size="medium">enviar</Button>
                    </ContactForm>
                </FormContainer>
            </GridContainer>
        </ContactContainer>
    );
}
 
export default Contact;