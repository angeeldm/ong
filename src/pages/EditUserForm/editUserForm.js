import React, { useEffect, useState } from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import InlineEditor from '@ckeditor/ckeditor5-build-inline';
import { useFormik } from 'formik';
import { TextField, Button } from '@material-ui/core';
import { FormContainer, Form, Title, FieldsContainer, NewsField, ContentField, FieldTitle, NewsImage } from './Styles';
import { useDispatch, useSelector } from 'react-redux';
// import { editNews, addNews } from '../../redux/News/newsReducer';
import { useParams, useHistory } from "react-router-dom";
import { alertSuccess, alertError } from '../../elements/Alert';
import { Put } from '../../services/HttpService';
import { update_user } from '../../redux/Profile/profileReducer';
import Backoffice from '../Backoffice/index'
import {GridBackoffice} from '../Profile/styles'

const NewsForm = () => {
    const fd = new FormData();
    const dispatch = useDispatch();
    const { id } = useParams();
    const history = useHistory();
    const loggedUser = useSelector(state => state.profile.loggedUser)
    const [values, setValues] = useState({
        firstName: '',
        lastName: '',
        email: '',
        image: [],
        password: ''
    })

    useEffect(() => {
        if (!id) {
            return;
        } else {
            setValues({
                firstName: loggedUser[0].firstName,
                lastName: loggedUser[0].lastName,
                email: loggedUser[0].email,
                image: loggedUser[0].image,
                password: '',
                confirmPassword: ''
            })
        }
    }, [])

    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            firstName: values.firstName,
            lastName: values.lastName,
            image: values.image,
            email: values.email,
            password: values.password,
            confirmPassword: values.confirmPassword
        },
        onSubmit: data => {
            if (data.firstName === '' || data.lastName === '') {
                alertError({ title: 'Los campos no deben ir vacios' })
                return
            } if (!id) {
                fd.append('firstName', data.firstName)
                fd.append('lastName', data.lastName)
                fd.append('image', data.image, data.image.name)
                fd.append('email', data.email)
                fd.append('password', data.password)
                // dispatch(addNews(fd));
                alertSuccess({ title: 'Usuario creado exitosamente' });
                history.push(`/backoffice/perfil/${loggedUser[0].id}`)
            } else {
                if (data.password !== data.confirmPassword) {
                    alertError({ title: 'Las contraseñas no coinciden' })
                } else {
                    fd.append('firstName', data.name)
                    fd.append('lastName', data.content)
                    fd.append('image', data.image)
                    fd.append('email', data.email)
                    fd.append('password', data.password)
                    fd.append('roleId', data.roleId)
                    // dispatch(editNews(fd, id));
                    // console.log(data)
                    Put(`/users/${loggedUser[0].id}`, data).then(res => {
                        /* DISPATCH PARA ACT STATE LOGGED USER*/
                        dispatch(update_user(data));
                    })
                        .catch(err => alertError({ title: 'Algo salio mal' }))
                    alertSuccess({ title: 'Perfil editado exitosamente' });
                    setTimeout(() => {
                        history.push(`/perfil/${loggedUser[0].id}`);
                    }, 1300)
                }

            }
        }
    })

    return (
        <>
            {
                !loggedUser ? <></> :
                    <GridBackoffice>
                        <Backoffice />
                        <FormContainer>
                            <Title>{id ? 'Editar' : 'Crear'} perfil de usuario</Title>
                            <Form onSubmit={formik.handleSubmit}>
                                <FieldsContainer>
                                    <NewsField>
                                        <FieldTitle>Nombre</FieldTitle>
                                        <TextField type="text" name="firstName" value={formik.values.firstName || ''} onChange={formik.handleChange} variant="outlined" fullWidth />
                                    </NewsField>
                                    <NewsField>
                                        <FieldTitle>Apellido</FieldTitle>
                                        <TextField type="text" name="lastName" value={formik.values.lastName} onChange={formik.handleChange} variant="outlined" fullWidth />
                                    </NewsField>
                                </FieldsContainer>
                                <FieldsContainer>
                                </FieldsContainer>
                                <FieldsContainer>
                                    <NewsField>
                                        <FieldTitle>Email</FieldTitle>
                                        <TextField type="text" name="email" value={formik.values.email} onChange={formik.handleChange} variant="outlined" fullWidth />
                                    </NewsField>
                                </FieldsContainer>
                                <FieldsContainer>
                                    <NewsField>
                                        <FieldTitle>Contraseña</FieldTitle>
                                        <TextField type="password" name="password" value={formik.values.password} onChange={formik.handleChange} variant="outlined" fullWidth />
                                    </NewsField>
                                    <NewsField>
                                        <FieldTitle>Repetir Contraseña</FieldTitle>
                                        <TextField type="password" name="confirmPassword" value={formik.values.confirmPassword} onChange={formik.handleChange} variant="outlined" fullWidth />
                                    </NewsField>
                                </FieldsContainer>

                                {/* <NewsField>
                    <FieldTitle>Imagen</FieldTitle>
                    {!id ?
                        <input type="file" name="image" onChange={(e) => {const imageValue = e.target.files[0];
                            formik.values.image = imageValue}}
                        />
                    :
                    <>
                        <NewsImage src={`/files/getfile/${formik.values.image}` || ''} alt=""/>
                        <input type="file" name="image" onChange={(e) => {
                            const imageValue = e.target.files[0];
                            formik.values.image = imageValue
                            }}
                        />
                    </>
                    }
                    
                </NewsField> */}

                                <Button type="submit" variant="contained" color="primary" size="medium">Editar</Button>
                            </Form>
                        </FormContainer>
                    </GridBackoffice>
            }

        </>
    );
}

export default NewsForm;
