import React from "react";
import WelcomeTitle from "../../Components/WelcomeTitle/WelcomeTitle";
import Novedades from "../../Components/Novedades/Novedades";
import { Container } from "@material-ui/core";
import Foto from "./Foto.jpg"

const Home = () => {
  return (
    <>
    <div style={{ width: "100%", minHeight: "300px", backgroundImage: `url(${Foto})`, backgroundRepeat: 'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover' }} ></div>
    <Container>
      <WelcomeTitle />
      <br />
      <br />
      <h2>Novedades</h2>
      <Novedades />
      <br />
      <br />
      <br />
      <br />
      <h2>Testimonios</h2>
      <Novedades />
    </Container>
    </>
  );
};

export default Home;
