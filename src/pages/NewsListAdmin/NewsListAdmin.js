import React, { useEffect } from 'react';
import { Get } from '../../services/HttpService';
import { useSelector, useDispatch } from 'react-redux';
import { get_news, deleteNews } from '../../redux/News/newsReducer';
import { alertError, alertConfirm, alertSuccess } from '../../elements/Alert';
import SpinnerLoader from '../../Components/LoaderSpinner';
import {Container, MainTitle, NewList, NewCard, NewTitle, NewInfo, NewImage, ButtonContainer, GridContainer} from './Styles';
import { Button } from "@material-ui/core";
import { FaTrash } from "react-icons/fa";
import { AiFillEdit } from "react-icons/ai";
import { useHistory } from "react-router-dom";
import Backoffice from '../Backoffice/index'


const NewsListAdmin = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const news = useSelector(store => store.news.news);

    useEffect(() => {
        Get('/news')
        .then(res => dispatch(get_news(res)))
        .catch(err => alertError({title: 'Hubo un error cargando las novedades', text: 'Por favor, intente nuevamente'}))
    }, [])

    const handleDelete = (id) => {
        alertConfirm({title: 'Seguro quiere eliminar esta novedad?', callback: () => {
            dispatch(deleteNews(id))
            alertSuccess({title: 'Se eliminó exitosamente'})
        }})
    }

    return (
        <GridContainer>
            <Backoffice />
            <Container>
                <MainTitle>Listado de Novedades</MainTitle>
                <Button variant="contained" color="primary" style={{margin: '10px'}} onClick={() => history.push('/backoffice/crear-novedad')}> Agregar Novedad </Button>
                {news.length > 0 ?
                    <NewList>
                        {news.map((item) => {
                            const dateString = item.createdAt;
                            const date = dateString?.split('T');
                            return(
                                <NewCard key={item.id}>
                                    <NewTitle>{item.name}</NewTitle>
                                    <NewInfo>Creado el: {date[0]}</NewInfo>
                                    <NewImage src={`/files/getfile/${item.image}`} alt="" />
                                    <ButtonContainer>
                                        <Button variant="contained" color="primary" onClick={() => history.push(`/backoffice/edit-novedad/${item.id}`)} ><AiFillEdit /></Button>
                                        <Button variant="contained" color="secondary" onClick={() => handleDelete(item.id)}><FaTrash /></Button>
                                    </ButtonContainer>
                                </NewCard>
                            )
                        })}
                    </NewList> 
                :   <SpinnerLoader />
                }
            </Container>
        </GridContainer>
    );
}
 
export default NewsListAdmin;