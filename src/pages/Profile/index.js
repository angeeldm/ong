import React, { useEffect, useState } from 'react'
import { useSelector } from "react-redux"
import { ProfileContainer, DataContainer, NameContainer, NameItem, EmailItem, ButtonContainer, ButtonItem, GridBackoffice, ImageUser  } from "./styles"
import SpinnerLoader from "../../Components/LoaderSpinner"
import { alertSuccess, alertConfirm } from "../../elements/Alert"
import Backoffice from '../Backoffice/index'
import imageEmptyUserDefault from './emptyImageUser.png'
import { Get } from '../../services/HttpService'

const Profile = () => {
  const dataList = useSelector(store => store.profile.loggedUser)

  const handleEdit = () => {
    alertSuccess({ title: "Edited" })
  }

  const handleDelete = () => {
    alertConfirm({ title: "¿Seguro que desea eliminar el perfil?", 
                  onConfirmText: "Perfil eliminado",
                  onDenyText: "Perfil no eliminado",
                  callback: () => {console.log("Perfil eliminado")} })
  }
  useEffect(() => {
  }, [])

  return (
    <GridBackoffice>
      <Backoffice />
      <ProfileContainer>
        {
          dataList.length < 0
            ? <SpinnerLoader />
            : <>
              <DataContainer>
                  <h1>Hola {!dataList ? "?" : dataList[0].firstName}</h1>
                {/* <NameContainer>
                  <NameItem>{dataList[0].name}</NameItem>
                  <NameItem>{dataList[0].lastname}</NameItem>
                </NameContainer> */}
                {/* <EmailItem>{dataList[0].email}</EmailItem> */}

                <ImageUser src={imageEmptyUserDefault} />
                  <h5>Tus datos: </h5>
                  <span>Nombre : {!dataList ? "" : dataList[0].firstName}</span>
                  <span>Apellido : {!dataList ? "" : dataList[0].lastName}</span>
                  <span>Email: {!dataList ? "" : dataList[0].email}</span>
              </DataContainer>
              {/* <ButtonContainer>
                  <ButtonItem onClick={handleEdit} isEdit>Editar perfil</ButtonItem>
                  <ButtonItem onClick={handleDelete}>Eliminar perfil</ButtonItem>
              </ButtonContainer> */}
            </>
        }
      </ProfileContainer>
    </GridBackoffice>
  )
}

export default Profile
