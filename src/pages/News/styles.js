import styled from "styled-components"

export const NewsGrid = styled.div`
  display: grid;
  justify-content: center;
  grid-template-columns: 1fr;
  text-align: center;
  width: 90%;
  max-width: 1024px;
  grid-gap: 20px;
  margin: 20px auto 0 auto;

  @media(min-width: 768px){
    grid-template-columns: repeat(2, 1fr)
  }

  @media(min-width: 998px){
    grid-template-columns: repeat(3, 1fr)
  }
`

export const NewsTitle = styled.h1`
  text-align: center;
  font-size: 40px;
  margin: 0;
  font-weight: bold;
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`