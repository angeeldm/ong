import React, { useState } from 'react'
import { Header, MenuIcon, CloseMenuIcon, NavButton, AdminTitle } from "./styles"
import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai"
import { NavLink } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { select_user_for_edit } from '../../redux/UserData/usersReducer'

const Backoffice = () => {
  const [show, setShow] = useState(false)
  const dispatch = useDispatch();
  const userSelected = useSelector(store => store.user.userSelected)
  const userLogged = useSelector(store => store.profile.loggedUser)

  const toggleShow = () => {
    setShow(!show)
  }

  const defaultList = [
    {
      name: "Inicio",
      path: `/perfil/${userLogged[0].id}`
    },
    {
      name: "Editar perfil",
      path: `/backoffice/editar-usuario/${userLogged[0].id}`
    },
    {
      name: "Crear Testimonio",
      path: `/backoffice/crear-testimonios`
    }
  ]

  const adminList = [
    {
      name: "Novedades",
      path: `/backoffice/novedad`,
    },
    {
      name: "Usuarios",
      path: "/backoffice/editar-usuarios",
    },
    {
      name: "Testimonios",
      path: "/backoffice/testimonios",
    }
  ]

  return (
    <>
      <Header isShow={show} show>
        {/* <CloseMenuIcon onClick={toggleShow}>{AiOutlineClose({size: 18})}</CloseMenuIcon> */}
        {
          defaultList.map(item => (
            <NavLink to={item.path} key={item.name}> <NavButton>{item.name}</NavButton> </NavLink>
          ))
        }
        {
          userLogged.length > 0 && userLogged[0].roleId === 1
            ? 
            <>
            <AdminTitle>Controles de admin</AdminTitle>
            {
              adminList.map(item => (
                <NavLink to={item.path} key={item.name}> <NavButton >{item.name}</NavButton> </NavLink>
              ))
            }
          </>
          : 
         null
        }
      </Header>
      {/* {
        show ? null
        : <MenuIcon onClick={toggleShow}>{AiOutlineMenu()}</MenuIcon>
      } */}
    </>
  )
}

export default Backoffice
