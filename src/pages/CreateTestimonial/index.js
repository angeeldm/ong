import React from 'react'
import TestimonialForm from '../../Components/TestimonialForm'
import Backoffice from '../Backoffice'
import { GridContainer } from '../NewsListAdmin/Styles'

const CreateTestimonial = () => {
  return (
    <GridContainer>
      <Backoffice />
      <TestimonialForm />
    </GridContainer>
  )
}

export default CreateTestimonial
