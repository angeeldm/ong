import React, { useEffect } from 'react';
import Logo from '../Header/logo.png'
import {FooterContainer, NavSpace} from './Styles';
import {useSelector, useDispatch} from 'react-redux';
import {getWebLinks, getSocialLinks} from '../../redux/Footer/footerReducer';
import { NavLink } from 'react-router-dom';

const Footer = () => {
    const dispatch = useDispatch();
    const webLinks = useSelector(store => store.footer.webLinks);
    const socialLinks = useSelector(store => store.footer.socialLinks);

    useEffect(() => {
        dispatch(getWebLinks());
        dispatch(getSocialLinks());
    }, [])

    return (
        <FooterContainer>
           <NavLink to="/"> <img width="50%" src={Logo} alt="Logo"/> </NavLink>

             
                <NavSpace>
                {webLinks.length > 0 ? 
                    webLinks.map((link) => {
                        return(
                            <NavLink key={link.pathname} to={link.path}>{link.pathname}</NavLink>
                        )
                    })
                    : ''
                }
                </NavSpace>

            <NavSpace icon>
                {socialLinks.length > 0 ? 
                    socialLinks.map((element) => {
                        return(
                            <a key={element.pathname} target="_blank" href={element.path}>{element.pathname}</a>
                        )
                    })
                    : ''
                }
            </NavSpace>
        </FooterContainer>
    );
}

export default Footer;