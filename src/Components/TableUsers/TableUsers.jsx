import React, { useEffect, useState } from "react";
import {
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
} from "@material-ui/core";
import { GridBackoffice  } from "../../pages/Profile/styles"
import DeleteIcon from "@material-ui/icons/Delete";
import { useDispatch, useSelector } from "react-redux";
import { Delete, Get, Put } from "../../services/HttpService";
import { alertError, alertSuccess } from "../../elements/Alert";
import { useHistory } from 'react-router-dom'
import SpinnerLoader from '../LoaderSpinner'
import Backoffice from '../../pages/Backoffice/index'
import { get_all_users, select_user_for_edit } from "../../redux/UserData/usersReducer";

const useStyles = makeStyles({
  table: {
    minWidth: "50%",
  },
});

const TableUsers = () => {
  const dispatch = useDispatch();
  const listOfUsers = useSelector(state => state.user.listOfUsers)
  const [values, setValues] = useState([{
    id:'',
    name: '',
    lastName: '',
    email: ''
  }])
  const classes = useStyles();
  // const table = useSelector((state) => state.table);
  let history = useHistory()
  const handleEdit = (usersId) => {
    const selectedUser = listOfUsers.filter(user => usersId === user.id)

    dispatch(select_user_for_edit(selectedUser))
    console.log(selectedUser)
    // history.push(`/backoffice/editar-usuario/${usersId}`)
  };

  const handleDelete = (id) => {
    Delete(`/users/${id}`).then(res => {
      Get(`/users/`).then(res =>{
        setValues(res)
        dispatch(get_all_users(res))
      } )
      alertSuccess({
        title: 'Success message ',
        text: 'the user its was deleted successfully'
      })
    })
  }


  useEffect(() => {
   Get(`/users/`)
      .then((res) => {
        setValues(res)
        dispatch(get_all_users(res))
      })
      .catch((error) => {
        alertError({ title: "There was an error", text: "Please, try again" })
      })
  },[])
  return (
    <>
    <GridBackoffice>
      <Backoffice />
      {!values ? <SpinnerLoader /> : 
      <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell>Apellido</TableCell>
            <TableCell>Email</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {!values ? (
            <TableRow></TableRow>
          ) : (
              values.map((users) => (
                <TableRow key={users.id}>
                  <TableCell>{users.firstName}</TableCell>
                  <TableCell>{users.lastName}</TableCell>
                  <TableCell>{users.email}</TableCell>
                <TableCell>
                    <Button onClick={() => handleDelete(users.id)}>
                    <DeleteIcon />
                  </Button>
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </TableContainer>
      }
      
      </GridBackoffice>
    </>

  );
};

export default TableUsers;
