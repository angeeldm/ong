import React from 'react'
import Header from "../Header/index"
import Footer from "../Footer/Footer"
import "../../App.css"
import Backoffice from '../../pages/Backoffice/index'
import { useSelector } from 'react-redux'

const Layout = ({ children }) => {
  const userLogged = useSelector(store => store.profile.loggedUser)

  return (
    <>
      <Header />
      <div className="App">
      {
        userLogged.length > 0 ?
        <>
          <Backoffice />
          { children }
        </>
        :
        <></>
      }
      </div>
      <Footer />
    </>
  )
}

export default Layout
