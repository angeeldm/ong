import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { add_list_action } from "../../redux/Header/headerReducer"
import { useHistory } from "react-router-dom"
import { dataMock } from "./dataMock";
import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai";
import {
  HeaderContainer,
  HeaderLogo,
  HeaderNav,
  NavLinks,
  NavAnchor,
  HeaderButtons,
  LoginButton,
  RegisterButton,
  Icon
} from "./styles"
import Logo from "./logo.png";
import useLocalStorage from "../../services/hooks/useLocalStorage"
import { logout_user } from "../../redux/Profile/profileReducer";

const Header = () => {
  const history = useHistory()
  const [show, setShow] = useState(false);
  const token = useLocalStorage()

  const dispatch = useDispatch()
  const header = useSelector(store => store.header)
  const userState = useSelector(store => store.profile.loggedUser)
  
  const [user, setUser] = useState()

  useEffect(() => {
    setUser(userState);
    dispatch(add_list_action(dataMock))
  }, []);

  const toggleMenu = (e) => {
    setShow(!show);
  };

  const handleClick = (id) => {
    history.push(`/perfil/${id}`)
  }

  return (
    <HeaderContainer>
      <HeaderLogo to="/">
        <img src={Logo} />
      </HeaderLogo>
      <Icon onClick={toggleMenu}>{AiOutlineMenu()}</Icon>
      <HeaderNav isShow={show}>
        <Icon onClick={toggleMenu}>{AiOutlineClose()}</Icon>
        
        <NavLinks>
            {header.list.map((item) => (
              <NavAnchor exact={true} key={item.name} activeClassName="active" to={item.path}>{item.name}</NavAnchor>
            ))}
        </NavLinks>
        
        <HeaderButtons>
          {
            userState.length > 0
              ?
              <>
                <LoginButton onClick={() => {
                  dispatch(logout_user)
                  history.push("/")
                }}>Cerrar sesion</LoginButton>
                <RegisterButton onClick={() => handleClick(userState[0].id)} >Perfil</RegisterButton>
              </>
              : 
              <>
              <LoginButton onClick={() => history.push("/iniciar")}>Iniciar</LoginButton>
              <RegisterButton onClick={() => history.push("/registro")} >Registrate</RegisterButton>
            </>
          }
        </HeaderButtons>
      </HeaderNav>
    </HeaderContainer>
  );
};


export default Header;
