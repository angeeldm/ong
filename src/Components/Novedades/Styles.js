import styled from "styled-components";

const Container = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    justify-content: center;
    align-items: center;
    gap: 20px;

    @media(min-width: 768px){
        grid-template-columns: repeat(2, 1fr);
    }

    @media(min-width: 998px){
        grid-template-columns: repeat(3, 1fr);
    }
`;

const CardNew = styled.div`
    margin: 0 auto;
`;

export {Container, CardNew}