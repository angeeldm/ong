import {
  Card,
  CardContent,
  Grid,
  makeStyles,
  Typography,
  CardActionArea,
  CardMedia,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { alertError } from "../../elements/Alert";
import { Get } from "../../services/HttpService";
import ReactHtmlParser from 'react-html-parser';
import {Container, CardNew} from './Styles'

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  pos: {
    marginBottom: 12,
  },
});

const Novedades = () => {
  const [values, setValues] = useState([{
    id: '',
    content: '',
    name: '',
    image: ''
  }])
  const classes = useStyles();
  
  useEffect(() => {
    Get(`/news/`)
      .then((res) => setValues(res))
      .catch((error) => {
        alertError({ title: "Hubo un error", text: "Intente nuevamente" })
      })
  }, [])

  return (
    <>
      <Container>
        {values.map(n => (
          <CardNew key={n.id} >
            <Card className={classes.root} >
              <CardActionArea >
                <CardMedia
                  image={`/files/getfile/${n.image}`}
                  alt={n.name}
                  height="120"
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {n.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {ReactHtmlParser(n.content)}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </CardNew>
        ))
        }
        </Container>
    </>
  );
};

export default Novedades;
