import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { alertError } from "../../elements/Alert";
import { getOrganization } from "../../redux/Organizations/organizationsReducer";
import { Get } from "../../services/HttpService";
const WelcomeTitle = () => {

  const [title, setTitle] = useState([{
    welcomeText: ''
  }])
  const dispatch = useDispatch();

  useEffect(() => {
    Get(`/organizations/1/public`)
      .then((res) => {
        setTitle(res)
        dispatch(getOrganization(res))
      })
      .catch((error) => {
        alertError({ title: "Hubo un error", text: "Intente nuevamente" })
      })
  }, [])
  
  return (
    <>
      <h1> {title.welcomeText} </h1>
    </>
  );
};

export default WelcomeTitle;
