import React, {useEffect, useState} from 'react'
import { NewLink, ImageWrapper, NameWrapper } from "./styles"
import LoaderSpinner from '../LoaderSpinner';

const NewItem = (props) => {
  const [isMounted, setIsMounted] = useState(false)
  useEffect(() => {
    setIsMounted(true)
  })

  return (
    <NewLink to={`/novedad/${props.id}`}>
      {
        !isMounted ?
          <LoaderSpinner />
        : 
        <NameWrapper>
          <ImageWrapper src={`/files/getfile/${props.image}`} alt=""/>
          <h2>{props.name}</h2>
        </NameWrapper>
      }
    </NewLink>
  )
}

export default NewItem
