import styled from "styled-components"
import { Link } from "react-router-dom"

export const NewLink = styled(Link)`
  display: flex;
  flex-direction: column;
  width: 100%;
  text-decoration: none;
  color: white;
  transition: .2s;
  border-radius: 5px;
  padding: 5px;
  :hover {
    box-shadow: 2px 2px 8px #b8b8b8;
    transition: .2s;
  }
`
export const ImageWrapper = styled.img`
  width: 100%;
  max-height: 90%;
  margin: 0 auto;
`

export const NameWrapper = styled.div`
  height: 260px;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  h2 {
    box-sizing: border-box;
    margin: 0;
    padding: 2px 0; 
    background-color: #18A0FB;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    font-size: 16px;
  }
`